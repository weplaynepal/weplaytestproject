import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class FirstState extends Equatable{
  @override
  List<Object> get props => [];
}

class EmptyFirstState extends FirstState{}

class LoadingFirstState extends FirstState{}

class OnSuccessFirstState extends FirstState{
  final Map<String,dynamic> formData;
  OnSuccessFirstState({@required this.formData});

  @override
  List<Object> get props => [this.formData];
}

class OnErrorFirstState extends FirstState{
  final String errorMessage;
  OnErrorFirstState({@required this.errorMessage});

  @override
  List<Object> get props => [this.errorMessage];
}