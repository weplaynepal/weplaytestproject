import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class FirstEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class HoldDataFirstEvent extends FirstEvent{
  final Map<String,dynamic> formData;
  HoldDataFirstEvent({@required this.formData});

  @override
  List<Object> get props => [this.formData];
}