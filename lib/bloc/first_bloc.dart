import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weplay_test_project/bloc/first_event.dart';
import 'package:weplay_test_project/bloc/first_state.dart';

class FirstBloc extends Bloc<FirstEvent, FirstState>{
  FirstBloc() : super(EmptyFirstState());

  @override
  Stream<FirstState> mapEventToState(FirstEvent event) async*{
    if(event is HoldDataFirstEvent){
      yield LoadingFirstState();
      try{
        yield OnSuccessFirstState(formData: null);
      }catch(e){
        yield OnErrorFirstState(errorMessage: 'Error in first bloc');
      }
    }
  }
}