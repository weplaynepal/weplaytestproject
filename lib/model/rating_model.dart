class RatingModel {
  String name;
  double value;
  List<String> interest;

  RatingModel({
    this.name,
    this.value,
    this.interest,
});

  factory RatingModel.fromServer(Map<String,dynamic> data){
    return RatingModel(
      name: data['name'],
      value: data['value'],
      ///todo : need to modify here.
      interest: data['interest'],
    );
  }

  Map<String,dynamic> toMap(){
    return {
      'name': name,
      'value': value,
      'interest': interest,
    };
  }
}