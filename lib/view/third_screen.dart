import 'package:flutter/material.dart';
import 'package:weplay_test_project/model/rating_model.dart';
import 'package:weplay_test_project/services/firestore_services.dart';

class ThirdScreen extends StatelessWidget {
  FirestoreServices firestoreServices = FirestoreServices();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Third Screen',
          ),
        ),
        body: Container(
          child: StreamBuilder<List<RatingModel>>(
            stream: null,
            builder: (context, snapshot) {
              return ListView.builder(
                itemCount: 0,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      'Place your server data with list tile',
                    ),
                  );
                },
              );
            }
          ),
        ),
      ),
    );
  }
}
