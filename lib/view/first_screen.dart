import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:weplay_test_project/bloc/first_bloc.dart';
import 'package:weplay_test_project/bloc/third_bloc.dart';
import 'package:weplay_test_project/view/second_screen.dart';

class FirstScreen extends StatelessWidget {
  final formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen'),
      ),
      body: Container(
        child: FormBuilder(
          key: formKey,
          child: Column(
            children: [
              FormBuilderTextField(
                attribute: 'name',
                decoration: InputDecoration(
                  labelText: 'Enter Name',
                ),
              ),
              SizedBox(
                height: 30,
              ),
              FormBuilderTextField(
                attribute: 'interest',
                decoration: InputDecoration(
                  labelText: 'Enter Your Interest',
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          //place your logic
          if(formKey.currentState.saveAndValidate()){
            final formValue = formKey.currentState.value;
            ///todo need to call first bloc here to pass form data to second screen
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (mContext) {
                  return BlocProvider.value(
                    value: BlocProvider.of<ThirdBloc>(context),
                    child: BlocProvider.value(
                      value: BlocProvider.of<FirstBloc>(context),
                      child: SecondScreen(),
                    ),
                  );
                },
              ),
            );
          }
        },
        icon: Icon(
          Icons.drive_file_move,
        ),
        label: Text('Continue'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
