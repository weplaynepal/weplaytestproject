import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weplay_test_project/bloc/first_bloc.dart';
import 'package:weplay_test_project/bloc/first_state.dart';
import 'package:weplay_test_project/bloc/third_bloc.dart';
import 'package:weplay_test_project/view/third_screen.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Second Screen',
        ),
      ),
      body: Container(
        child: Center(
          child: Column(
            children: [
              Text(
                'Data from first screen:',
              ),
              BlocBuilder<FirstBloc,FirstState>(
                builder: (context, firstState) {
                  return Text(
                    'Show your data here...',
                  );
                },
              ),
              Text(
                'Data from third screen:',
              ),
              BlocBuilder<ThirdBloc,String>(
                builder: (context, dataFromThirdString) {
                  return Text(
                    '$dataFromThirdString',
                  );
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Go To Third Screen'),
        onPressed: (){
          ///todo accept data from third screen here and update above in third screen data using third bloc
          Navigator.push(context,MaterialPageRoute(
            builder: (mContext)=> ThirdScreen(),
          ),);
        },
      ),
    );
  }
}
