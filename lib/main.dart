import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weplay_test_project/bloc/first_bloc.dart';
import 'package:weplay_test_project/bloc/third_bloc.dart';
import 'package:weplay_test_project/view/first_screen.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_){
            return FirstBloc();
          },
        ),
        BlocProvider(
          create: (_){
            return ThirdBloc();
          },
        ),
      ],
      child: MaterialApp(
        title: 'WePlay Test Project',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: FirstScreen(),
      ),
    );
  }
}
